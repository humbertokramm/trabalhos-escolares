import skrf as rf
import matplotlib.pyplot as plt
from numpy import pi

ntwk = rf.Network('antena.s1p')
ntwk.plot_s_smith()
plt.show()

# Obtem o valor da impedância
[[[z]]] = ntwk['0.91ghz'].z
print('Z em 0.91ghz',z)

#Calculo do Indutor relativo à antena
f = 910e6
L = z.imag/(2*pi*f)
print(L)
