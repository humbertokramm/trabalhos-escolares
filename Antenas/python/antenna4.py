
from numpy import cos, sin, linspace,meshgrid, pi,append,log10, round, degrees
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as axes3d
import matplotlib as mpl

pontos = 50
theta = linspace(0, pi, pontos)
phi = append(linspace(0, pi/2, int(pontos/2)),linspace(3*pi/2, 2*pi, int(pontos/2)))

THETA, PHI = meshgrid(theta, phi)

R = abs(sin(THETA)*(cos(PHI)**2))
X = R * sin(THETA) * cos(PHI)
Y = R * sin(THETA) * sin(PHI)
Z = R * cos(THETA)

fig = plt.figure()
ax = fig.add_subplot(1,1,1, projection='3d')
plot = ax.plot_surface(
	X, Y, Z,  rstride=1, cstride=1, cmap=plt.get_cmap('jet'),
	linewidth=0, antialiased=False, alpha=0.5)


fake2Dline = mpl.lines.Line2D([0],[0])

ax.legend([fake2Dline], ['MAX: X = '+str(round(X.max(),2))+' Y = '+str(round(Y.max(),2))+' Z = '+str(round(Z.max(),2))])

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')





### Largura do Feixe em XY

pontos = 150
theta = linspace(0, pi, pontos)
phi = append(linspace(0, pi/2, int(pontos/2)),linspace(3*pi/2, 2*pi, int(pontos/2)))

theta = theta[1:-1]
R = (sin(theta)*(cos(0)**2))
gain = 20*log10(R)


index = []
for i in range(len(gain)):
	if round(gain[i],1) == -3.1:
		index.append(i)

HPBWa = [theta[index[0]],theta[index[1]]]
HPBWg = [gain[index[0]],gain[index[1]]]

fig2 = plt.figure()
ax2 = fig2.add_subplot(projection='polar')
ax2.plot(theta, gain)
ax2.plot(HPBWa, HPBWg,label = 'Largura do feixe XY = '+str(round(degrees(HPBWa[1]-HPBWa[0]),1))+"°")
ax2.set_rticks([-20, -15, -10, -5])
ax2.set_rlabel_position(45)
ax2.legend()


### Largura do Feixe em XZ


R = (sin(pi/2)*(cos(phi)**2))
gain = 20*log10(R)

index = []
for i in range(len(gain)):
	if round(gain[i],1) == -3.0:
		index.append(i)
	if gain[i] < -30: gain[i] = -30

HPBWa = [phi[index[0]],phi[index[1]]]
HPBWg = [gain[index[0]],gain[index[1]]]

fig3 = plt.figure()
ax3 = fig3.add_subplot(projection='polar')
ax3.plot(phi, gain)
ax3.plot(HPBWa, HPBWg,label = 'Largura do feixe XZ = '+str(round(abs(degrees(HPBWa[1]-HPBWa[0])-360),1))+"°")
ax3.set_rticks([-20, -15, -10, -5])
ax3.set_rlabel_position(45)
ax3.legend()


plt.show()

