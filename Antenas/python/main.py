from printSI import printSI

trabalho = ''

c = 299792458 #m/s
freq = 900E6 #Hz
Λ = c/freq
D = 1e-3 #mm

trabalho += printSI(c,'c','','ms',latexEQU = 'c  ')
trabalho += printSI(freq,'freq','M','Hz',latexEQU = 'freq')
#trabalho += printSI(D,'D','m','m',latexEQU = 'D  ')
trabalho += printSI(Λ,'Λ','m','m',latexEQU = 'Λ = c/freq')

'''
R = (Λ/2) / (D)
trabalho += printSI(R,'R','','',latexEQU = 'R = (Λ/2) / D')

k= 0.9787 - 11.86497 / ((1 + (R/0.000449)**1.7925)**0.3)
trabalho += printSI(k,'k','','',latexEQU = 'k=  0.9787 - 11.86497 / ((1 + (R/0.000449)**1.7925)**0.3)')


L = k * c / (freq*2)
trabalho += printSI(L,'L','m','m',latexEQU = 'L = k * c / (freq*2)')

dipolo = L/2

trabalho += printSI(dipolo,'dipolo','m','m',latexEQU = 'dipolo = L/2')
'''

L_min = 0.47*Λ
trabalho += printSI(L_min,'L_min','m','m',latexEQU = 'L_min = 0.47*Λ')

L_max = 0.48*Λ
trabalho += printSI(L_max,'L_max','m','m',latexEQU = 'L_max = 0.48*Λ')


print(trabalho)