from numpy import cos, sin, linspace
from numpy import meshgrid, pi, round
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as axes3d
import matplotlib as mpl

# Seta os parametros de projeto
Bx = 0
By = 0
cdo = 3/3.5
k = 2 * pi / cdo
dx = cdo / 2
dy = cdo / 2
M = 3
N = 3
NDPZ = 1e-20 # Nao Divide Por Zero
pontos = 100
Ψ = 0
# Obtem o espaco linear
theta = linspace(0, pi/2, pontos)
phi = linspace(0, 2*pi, pontos)
THETA, PHI = meshgrid(theta, phi)

# Prepara Psi em X e Y
psix = k * dx * sin(THETA) * cos(PHI) + Bx
psiy = k * dy * sin(THETA) * sin(PHI) + By

# Calcula o fator de forma
AFx = sin((M/2)*psix)/(NDPZ+sin(psix/2))/M
AFy = sin((N/2)*psiy)/(NDPZ+sin(psiy/2))/N
AF = AFx*AFy
# Monta os valores X,Y,Z
R = abs(AF)
X = R * sin(THETA) * cos(PHI)
Y = R * sin(THETA) * sin(PHI)
Z = R * cos(THETA)

# Plota o Grafico
fig = plt.figure()
ax = fig.add_subplot(1,1,1, projection='3d')
plot = ax.plot_surface(
	X, Y, Z, rstride=1, cstride=1,
	cmap=plt.get_cmap('jet'))
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
plt.show()

