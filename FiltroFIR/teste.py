import matplotlib.pyplot as plt
import numpy as np

def abs_t(value):
    if type(value) == type([]):
        a = []
        for v in value:
            a.append((v.real**2 + v.imag**2)**(1/2))
    else:
        a = (value.real**2 + value.imag**2)**(1/2)
    return a


Fs = 500;  # taxa de amostragem
Ts = 1.0/Fs; # periodo de amostragem
t = np.arange(0,1,Ts) # vetor de tempo

f1 = 10;   # frequencia do sinal 1
x1_n = np.cos(2 * np.pi * f1 * t + 0)

f2 = 20;   # frequencia do sinal 2
x2_n = np.cos(2 * np.pi * f2 * t + 0)

x_n = x1_n + x2_n

N = len(x_n) # tamanho do sinal
k = np.arange(N) #vetor em k
T = N/Fs
frq = k/T # os dois lados do vetor de frequencia
frq = frq[range(int(N/2))] # apenas um lado

y=[]

for k in range(N):
    temp = 0
    
    for n in range(N):
        temp+= ( x_n[n] * np.exp((-1j * 2 * np.pi * n * k) / N ) )
    y.append (temp)

Y = np.array(y)    
Y = Y[range(int(N/2))]

#for k in range(N):
#    X[N] = np.sqrt( cos(y[k])**2 + sen(y[k])**2 ) )


fig, ax = plt.subplots(2, 1)
ax[0].plot(t,x_n)
ax[0].set_xlabel('Tempo')
ax[0].set_ylabel('Amplitude')
ax[1].plot(frq,abs_t(Y),'r')
ax[1].set_xlabel('Freq (Hz)')
ax[1].set_ylabel('|X(freq)|')

plt.show()
