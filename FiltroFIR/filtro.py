'''

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

'''
import numpy as np
import matplotlib.pyplot as plt

def FiltroFIR(filtro, sinal):
	#ajusta o tamanho da saída
	saida = np.zeros(len(filtro)+len(sinal)-1)
	for k in range(0,len(filtro)):
		soma = 0 #limpa para a proxima soma
		for n in range(0,len(sinal)):
			#equação do somatorio
			saida[k+n] += sinal[n]*filtro[k]
	#Ajusta o tempo para sincronizar a entrada com a saída
	tempoSaida = np.arange(0,len(saida))
	tempoSinal = np.arange(0,len(saida),(len(saida)-1)/(len(sinal)-1))

	return saida , tempoSaida , tempoSinal
	
#Gera o Sinal de Entrada
x = []
for k in range(0,10):
	x.append(k**2)
	#x.append(np.sin(k*np.pi*2))
print('x')
print(x)

'''
Fs = 300;  # taxa de amostragem
Ts = 1.0/Fs; # periodo de amostragem
t = np.arange(0,1,Ts) # vetor de tempo

f = 5;   # frequencia do sinal
x1_n = np.sin(2*np.pi*f*t + 0)
f = 100;
x2_n = np.sin(2*np.pi*f*t + 180)
x = x1_n + x2_n
'''

#Filtro 1
b = [0.4,0.4]
y,tempoY,tempoX = FiltroFIR(b,x)
print('b')
print(b)
print('y')
print(y)
#Grafico(y,tempoY,x,tempoX,"Filtro:[0.4,0.4]")

#Filtro 2
b = [0.2,0.2,0.2,0.2]
y,tempoY,tempoX = FiltroFIR(b,x)
print('b')
print(b)
print('y')
print(y)
#Grafico(y,tempoY,x,tempoX,"Filtro:[0.2,0.2,0.2,0.2]")

#Filtro 3
b = [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1]
y,tempoY,tempoX = FiltroFIR(b,x)
print('b')
print(b)
print('y')
print(y)
#Grafico(y,tempoY,x,tempoX,"Filtro:[0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1]")
