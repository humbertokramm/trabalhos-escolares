print('')
print('*************************')
print('Parâmetros do Projeto')
freq = 50e3 # kHz 
print('freq = ',freq, 'Hz')
Iomin = 0.1 #A
print('Iomin = ',Iomin, 'A')
Io = 1 #A
print('Io = ',Io, 'A')
Vo = 5 #V +/- 0,1 V
print('Vo = ',Vo, 'V')
delta_Vc = 0.1 #V
print('delta_Vc = ',delta_Vc, 'V')
Vin = VinMax = VinMin = 12 #Volts fixo.(sem faixa de valor máximo e mnimo)
print('Vin = ',Vin, 'V')
print('VinMax = ',VinMax, 'V')
print('VinMin = ',VinMin, 'V')
print('')
print('*************************')
print('Informações do Projeto')
Vce_sat = 1 #V
print('Vce_sat = ',Vce_sat, 'V')
Vd = 0.5 #V
print('Vd = ',Vd, 'V')

print('*************************')
print('Calculado')
dMax =  (Vo + Vd) / ( VinMax - Vce_sat + Vd)
dMin = (Vo + Vd) / ( VinMax - Vce_sat + Vd)
print('dMax = ',dMax*100,' %')
print('dMin = ',dMin*100,' %')


L = (dMin*(1-dMin)*VinMax*(1/freq)) / (2*Iomin);
print('L = ',L*1000000,' uH')

C = 10* (dMin*(1-dMin)*VinMax) / (8*L*delta_Vc*freq**2)
print('C = ',C*1000000,' uF')