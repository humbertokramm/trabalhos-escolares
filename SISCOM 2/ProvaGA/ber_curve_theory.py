import matplotlib.pyplot as plt
import numpy as np
from scipy import special as sp

# Q-function
def qfunc(x):
    return 0.5-0.5*sp.erf(x/np.sqrt(2))
    

# BER
def ber_calc(M, SNR):
    pspn = 10**(SNR/10)
    q_arg = np.sqrt((6*2*pspn)/((M**2)-1))
    ret = 2 * (1 - (1/M)) * qfunc(q_arg)
    return ret

def ber_curve(M):
    ber_curve = []
    snr_db = np.arange(-5, 15, 0.1)
    for sn in snr_db:
        ber_curve.append(ber_calc(M, sn))
    return ber_curve, snr_db

    
ber_cur_2, snr_db = ber_curve(2)
ber_cur_4, snr_db = ber_curve(4)

# plotting the points
plt.plot(snr_db, ber_cur_2, label='PAM-2')
plt.plot(snr_db, ber_cur_4, label='PAM-4')
plt.legend(loc='upper right')

# Logarithmic scale
plt.yscale('log')
plt.grid(True,which="both", linestyle='--')

# naming the x axis
plt.xlabel('SNR[dB]')
# naming the y axis
plt.ylabel('BER')

# giving a title to my graph
#plt.title('Bit Error Rate (BER)')

#plt.grid()

# function to show the plot
plt.show()

    