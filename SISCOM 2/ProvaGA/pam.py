import numpy as np    
import matplotlib.pyplot as plt
from mpam import ( LUT, upSample, LUTGray, pulseShapeFilter, downSample, insertSample, 
thresholdDetector, stringFromSymbols, stringFromBitStream, getA, addNoise )

from mat4py import loadmat
mat = loadmat('RxBinaryPAM.mat')

# Define input sinal
#sig_in_ = np.array([0, 1, -1, -1, 1])
# Input signal reduction factor.
#sig_in_ = sig_in_ * 0.25
sig_in = mat["RxBinaryPAM"][1]

# Define up sample
n_up_sample = 16
#n_samples = len(sig_in) * n_up_sample
n_samples = len(sig_in)

# Bits in char
bits_in_char = 7

# SNR
SNR = 5

# Definição da modulação, M = número de simbolos e A = amplitude entre simbolos
M = 4
A = 1/M

# Up sample block
#sig_us_ = []
#for s in sig_in_:
#    a = s * np.ones(n_up_sample)
#    sig_us_ = np.concatenate((sig_us_, a), axis=None)
# End up sample

#n_samples = len(sig_us_)
#sig_in = sig_us_

# Pulse Shaping Filter
sig_filter = pulseShapeFilter(sig_in, n_up_sample)
# End Pulse Shaping Filter

# Detector
bits_out = []
level = 4 * 0.5
for i in range(n_samples):
    if(((i+1) % n_up_sample) == 0):
        if(sig_filter[i] >= level):
            bits_out.append(1);
        elif(sig_filter[i] <= (-1*level)):
            bits_out.append(0);
#print('bits_out: ',bits_out)
# End Detecor

# Extract ASCII from binary
demo_string = stringFromBitStream(bits_out, bits_in_char)
print("demo_string:", demo_string)

# Cria símbolos
symbols = LUT(bits_out, M, A)
#symbols = LUTGray(bits_out,M, A)

# Up sample
s, t = upSample(symbols, n_up_sample)

# Insere amostragem
#s, t = insertSample(s, 0, 16, 0.0)

# Filtro de saída
s_tx = pulseShapeFilter(s, n_up_sample)
#s_tx = s

# Meio físico
# Add noise
s_medium = addNoise(s_tx, A, SNR)
#s_medium = s_tx

# Filtro de entrada
s_rx = pulseShapeFilter(s_medium, n_up_sample)
#s_rx = s_tx

# Down sample, com deslocamento de fase
s_d = downSample(s_rx, n_up_sample, 15+8)

# Detector
rx_symbols = thresholdDetector(s_d, M, getA(s_d, M))
#rx_symbols = thresholdDetector(s_d, M, A*4*4)
#print("rx_symbols: ", rx_symbols)

string = stringFromSymbols(rx_symbols, M, bits_in_char)
print("String Demo:", string)

plt.figurefigsize = (8, 4);
plt.step(t,s)
plt.plot(t,s_medium)
plt.plot(t,s_rx)
#plt.plot(np.arange(0, len(sig_in), 1), sig_in)
plt.xlabel('Amostras');
plt.ylabel('Simbolos')
plt.grid()
plt.show()
