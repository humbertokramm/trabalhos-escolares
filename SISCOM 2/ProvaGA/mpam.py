from numpy import log2, arange, sqrt, arange, array_split
from random import random

# Extract ASCII from binary
def stringFromBitStream(bits_in, bits_range_char):
    num_char = int(len(bits_in)/bits_range_char)
    #resize
    bits_in = bits_in[0:num_char*bits_range_char]
    bits_array = array_split(bits_in, num_char)
    string = ''
    for i in range(num_char):
        char_int = 0
        for b in range(bits_range_char):
            char_int += bits_array[i][b] * 2**(bits_range_char-b-1)
        string += chr(char_int)

    return string

# Pega bits apartir inteiros, 
def bitsFromInts(ints_in, M):
    bits_range = int(log2(M))
    bits_out = []
    for int_ in ints_in:
        for i in range(bits_range):
            bit = (int_ >> (bits_range-i-1)) & 1
            bits_out.append(bit)
    return bits_out
    
# Pega amplitude do pulso em  função do pico maximo e M
def getA(sig_in, M):
    max_ = 0
    for s in sig_in:
        val = abs(s)
        if val > max_:
            max_ = val
    return max_ / (M-1)
    
# Pega inteiros apartir arranjo de bits
def getIntsFromBits(bits_in, bits_range):
    ints_out = []
    # Padding zero
    bist_in_len = len(bits_in)
    res = bist_in_len % bits_range
    if res > 0:
        padding = bits_range - res
        bist_in_len += padding
        for p in range(padding):
            bits_in.append(0);
    # Get ints
    for b in range(0, bist_in_len, bits_range):
        int_ = 0
        for i in range(bits_range):
            shift = bits_range-1-i
            if shift > 0:
                base = shift << 1
            else:
                base = 1
            int_ += bits_in[b+i] * base
        ints_out.append(int_)
    return ints_out
    
#Tabela de simbolos
def tableSymbols(M, A):
    stop = M*A
    start = -stop+A
    step = 2*A
    return arange(start, stop, step)
    
def tableGray(M):
    #BinaryToGray
    table = arange(M)
    table_rever = arange(M)
    for i in range(0, M):
        table[i] = (i ^ (i >> 1))
    for t in table:
        table_rever[table[t]] = t 
    return table, table_rever    
    

#Transforma uma lista de binário em símbolos
#look-up tables
def LUT(bits_in, M, A):
    N = log2(M)
    if not N.is_integer():
        return "não é um valor válido de M"
    N = int(N)
    
    #Pega tabela de simbolos
    table = tableSymbols(M, A)
    
    #Converte valores para simbolos
    output = getIntsFromBits(bits_in, N)
    simbolos = []
    for i in output:
        simbolos.append(table[i])
    
    return simbolos
    
def LUTGray(bits, M, A):
    N = log2(M)
    if not N.is_integer():
        return "não é um valor válido de M"
    N = int(N)
    #Monta lista de valores
    valores = []
    for i in range(0,len(bits), N):
        valores.append(''.join(str(x) for x in bits[i:i+N]))
    #Converte valores para decimal
    output = []
    for i in valores:
        output.append(int(i,2))
        
    # Mapea a simbolos segundo codificação Gray
    table, table_rever = tableGray(M);
    
    #Converte valores para simbolos
    simbolos = []
    for i in output:
        # Mapea gray code
        sym = table_rever[i]
        # Deslocamento e amplitudo do simbolo.
        sym = 2*A*(sym-((M-1)/2))
        simbolos.append(sym)
    
    return simbolos
    
# Pulse Shaping Filter
def pulseShapeFilter(sig_in, n_up_sample):
    n_samples = len(sig_in)
    bn = sqrt(1/n_up_sample)
    sig_filter = []
    for i in range(n_samples):
        y = 0
        s = sig_in[i]
        for j in range(n_up_sample):
            ni = i - j
            if(ni < 0):
                zn = 0
            else:
                zn = sig_in[ni]
            y = y + (bn * zn)
        sig_filter.append(y)
    return sig_filter
# End Pulse Shaping Filter    
    

#Aumenta o numero de amostras
def upSample(lista,N):
    novaLista =[]
    for i in lista:
        for j in range(N):
            novaLista.append(i)
    t = list(range(len(novaLista)))
    return novaLista, t

# Reduz o número de amostras
def downSample(sig_in, num_down, shift_samples = 0):
    
    sig_in_len = len(sig_in)
    num_samples = int(sig_in_len / num_down)
    #if(sig_in_len % num_down):
    #    num_samples += 1)
    
    sig_out = []
    for s in range(num_samples):
        samp_index = s * num_down + shift_samples
        if(samp_index >= (sig_in_len-1)):
            sig_out.append(sig_in[sig_in_len-1])
            break
        else:
            sig_out.append(sig_in[s * num_down + shift_samples])
    return sig_out

def insertSample(sig_in, pos, num, val):
    for i in range(num):
        sig_in.insert(pos, val)
    return sig_in, arange(0, len(sig_in))
    
def thresholdDetector(sig_in, M, A):
    # Pega tabela de simbolos
    table = tableSymbols(M, int(A))
    # Pega o simbolo
    symbols = []
    for i in range(len(sig_in)):
        for a in range(M):
            # Limite superior
            if(a == (M-1)):
                if sig_in[i] >= (table[a]-A):
                    symbols.append(a)
                    break
            # Limite inferior
            elif(a == 0):
                if sig_in[i] < (table[a]+A):
                    symbols.append(a)
                    break
            # Valores internos
            else:
                sym = table[a]
                sig = sig_in[i]
                if sig < (sym+A) and sig >= (sym-A):
                    symbols.append(a)
                    break
    return symbols
    
def stringFromSymbols(simbols, M, range_bits_char):
    bits_in = bitsFromInts(simbols, M)
    return stringFromBitStream(bits_in, range_bits_char)
    
# Aplica ruido
def addNoise(sig_in, A, SNR):
    Anoise = A / 10**(SNR/20)
    sig_out = []
    for sig in sig_in:
        s = sig + (random() * Anoise)
        sig_out.append(s)
    return sig_out
