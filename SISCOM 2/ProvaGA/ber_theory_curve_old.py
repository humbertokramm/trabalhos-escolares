from scipy import special as sp
import numpy as np
import matplotlib.pyplot as plt


def qfunc(x):
    return 0.5-0.5*sp.erf(x/np.sqrt(2))
    
def ser(M, SNR):
    q_arg = np.sqrt( (6/((M**2)-1))*(2*SNR) )
    print("q_arg: ", q_arg)
    ret = 2 * (1-(1/M)) * qfunc(q_arg)
    
#definições do MPAM
M = 4

start_snr = 0
stop_snr = 15
step_snr = 1
n_snr = np.arange(start_snr, stop_snr, step_snr)

ber = []
for snr in n_snr:
    ber.append(ser(M, snr))
print("ber: ", ber)

plt.figurefigsize = (8, 4);
plt.plot(n_snr, ber)
plt.xlabel('SNR[dB]');
plt.ylabel('BER')
plt.grid()
plt.show()    