import matplotlib.pyplot as plt
import numpy as np
from random import choice
from mpam import ( LUT, upSample, pulseShapeFilter, downSample, thresholdDetector, 
    stringFromBitStream, getA, addNoise, bitsFromInts )

# loopback MPAM
def loopback(bits_in, M, A=1, SNR = 15):
    n_up_sample = 16
    # Cria símbolos
    symbols = LUT(bits_in, M, A)
    # Up sample
    s, t = upSample(symbols, n_up_sample)
    # Filtro de saída
    s_tx = pulseShapeFilter(s, n_up_sample)
    # Meio físico
    # Add noise
    s_medium = addNoise(s_tx, 4*A, SNR)
    # Filtro de entrada
    s_rx = pulseShapeFilter(s_medium, n_up_sample)
    # Down sample, com deslocamento de fase
    s_d = downSample(s_rx, n_up_sample, 15+7)
    # Detector
    #rx_symbols = thresholdDetector(s_d, M, getA(s_d, M))
    rx_symbols = thresholdDetector(s_d, M, 16*A)
    # Saida bits
    return bitsFromInts(rx_symbols, M)

def bitsRand(num_bits):
    bits_rand = []
    stt = [0,1]
    for i in range(num_bits):
        bits_rand.append(choice(stt))
    return bits_rand
    
def ber_calc(bits_tx, bits_rx):
    count_diff = 0
    # Verifica o tamanho valido.
    len_in = len(bits_tx)
    len_out = len(bits_rx)
    if(len_in < len_out):
        len_bits = len_in
    else:    
        len_bits = len_out
    # Confronta cada bit
    for i in range(len_bits):
        if(bits_tx[i] != bits_rx[i]):
            count_diff += 1
    return (count_diff/len_bits)

#for i in range(10):
#    print("bitsRand-{}".format(i), bitsRand(10))

# Curva BER em função do SNR

#definições do MPAM
def ber_curve(M, bits_rand):
    #M = 4
    A = 1/M

    start_snr = -5
    stop_snr = 15
    step_snr = 0.2
    n_snr = np.arange(start_snr, stop_snr, step_snr)

    ber_snr = []
    for snr in n_snr:
        bits_out = loopback(bits_rand, M, A, snr)
        #print("bits_rand: ", bits_rand)
        #print("bits_out: ", bits_out)
        ber = ber_calc(bits_rand, bits_out)
        #print("ber: ", ber)
        ber_snr.append(ber)
    return ber_snr, n_snr

bits_rand = bitsRand(1000)
ber_cur2, snr_db = ber_curve(2, bits_rand)
ber_cur4, snr_db = ber_curve(4, bits_rand)

plt.figurefigsize = (8, 4);

plt.plot(snr_db, ber_cur2, label='PAM-2')
plt.plot(snr_db, ber_cur4, label='PAM-4')
plt.legend(loc='upper right')

plt.xlabel('SNR[dB]');
plt.ylabel('BER')

# Logarithmic scale
plt.yscale('log')
plt.grid(True,which="both", linestyle='--')
#plt.grid()

plt.show()    

    

