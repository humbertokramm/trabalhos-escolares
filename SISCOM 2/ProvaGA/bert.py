from mpam import ( LUT, upSample, pulseShapeFilter, downSample, thresholdDetector, 
    stringFromBitStream, getA, addNoise )

# Definições
n_up_sample = 16

# loopback MPAM
def loopback(bits_in, M, A=1, SNR = 15):
    # Cria símbolos
    symbols = LUT(bits_out, M, A)
    
    # Up sample
    s, t = upSample(symbols, n_up_sample)

    # Filtro de saída
    s_tx = pulseShapeFilter(s, n_up_sample)

    # Meio físico
    # Add noise
    s_medium = addNoise(s_tx, A, SNR)

    # Filtro de entrada
    s_rx = pulseShapeFilter(s_medium, n_up_sample)

    # Down sample, com deslocamento de fase
    s_d = downSample(s_rx, n_up_sample, 15+8)

    # Detector
    rx_symbols = thresholdDetector(s_d, M, getA(s_d, M))
    
    # Saida bits
    return bitsFromSymbols(rx_symbols, M, )
    