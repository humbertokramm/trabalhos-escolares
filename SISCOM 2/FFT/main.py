import numpy as np
import matplotlib.pyplot as plt
import dm
import array
from pprint import pprint


# Sinal
A = 2.5			# Amplitude
f = 3e3			# frequencia
fase = 0		# Fase

# Parâmetros
n = 5			# Numero de Senoides
Fs = 8e3#500*f		# taxa de amostragem
Ts = 1.0/Fs		# periodo de amostragem


# Exibição
t_min = 0
t_max = n/f

# Montagem
t = np.arange(t_min,t_max,Ts) # vetor de tempo
sinal = A*np.sin(2 * np.pi * f * t + fase)

tR = np.arange(t_min,t_max,Ts/100) # vetor de tempo
xR = A*np.sin(2 * np.pi * f * tR + fase)

# modulação delta
badValue = True
i = 70
while badValue:
	i -= 1
	deltaV = A*Fs/f/i
	raw = dm.encode_dm(sinal,deltaV)
	
	yDelta = []
	lastValue = 0
	for v in raw:
		if v:
			lastValue += deltaV
			yDelta.append(lastValue)
			yDelta.append(lastValue)
		else:
			lastValue -= deltaV
			yDelta.append(lastValue)
			yDelta.append(lastValue)
	
	print(i,deltaV,max(yDelta[len(yDelta)//2:]))
	if max(yDelta[len(yDelta)//2:]) > A:
		badValue = False
	if i == 1:
		badValue = False


# Aplica o decoded
sinalDecode = dm.decode_dm(raw,deltaV)


# gráfico para delta
t_rise_fall = 1e-12 
deltaT = t[1]-t[0]
tstart = t - deltaT/2
tend = t + deltaT/2 - t_rise_fall

tDelta = np.concatenate((tstart,tend), axis=0)
tDelta = np.sort(tDelta, axis=None)
tDelta[0] = t_min
tDelta[-1] = t[-1]




#exit()

# Ajusta a escala do tempo para ficar bonito
if t[-1]-t[0] < 1e-3:
	t *= 1e6
	tDelta  *= 1e6
	tR  *= 1e6
	escala = ' [us]'
elif t[-1]-t[0] < 1:
	t *= 1e3
	tDelta *= 1e3
	tR  *= 1e3
	escala = ' [ms]'
else: 
	escala = ' [s]'

# Inicia o gráfico
fig, ax = plt.subplots(5, 1)

ax[0].set_ylabel('Sinal')
ax[0].set_title('f amostral = '+str(int(Fs/1e3))+'kHz / Delta V = '+str(round(deltaV,2))+'V')
ax[0].plot(t,sinal)

ax[1].set_ylabel('Delta')
ax[1].plot(tR,xR)
ax[1].plot(tDelta,yDelta)

ax[2].set_ylabel('Dados')
ax[2].stem(t,raw,markerfmt =' ')

ax[3].set_ylabel('Decoded')
ax[3].plot(t,sinalDecode)

ax[4].set_ylabel('Erro[%]')
ax[4].plot(t,(sinal-sinalDecode)*100/A)

# Legendas
for i in range(len(ax)):
	ax[i].grid(True,which = 'major', axis = 'both')
ax[-1].set_xlabel('Tempo'+escala)





from numpy.fft import fft, ifft

fft_t = tR
fft_x = xR

X = fft(fft_x)
N = len(fft_x)
n = np.arange(N)
T = N/f
freq = n/T 

plt.figure(figsize = (12, 6))
plt.subplot(121)

plt.stem(freq, np.abs(X), 'b', \
         markerfmt=" ", basefmt="-b")
plt.xlabel('Freq (Hz)')
plt.ylabel('FFT Amplitude |X(freq)|')
plt.xlim(0, 5000)

plt.subplot(122)
plt.plot(fft_t, ifft(X), 'r')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.tight_layout()
plt.show()



plt.show()