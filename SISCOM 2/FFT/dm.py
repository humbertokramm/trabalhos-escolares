import array

BYTES = 2   # N bytes arthimetic
MAX = 2 ** (BYTES * 8 - 1) - 1
MIN = - (2 ** (BYTES * 8 - 1)) + 1

print(MIN, MAX)

def encode_dm(samples, delta = MAX//21):
	"""
	Encodes audio bytearray data with Delta Modulation. Return a BitStream in a
	list
	Keyword arguments:
		samples -- Signed 16 bit Audio data in a byte array
		delta - Delta constant of DM modulation
	"""
	raw = samples
	'''raw = array.array('h')
	raw.frombytes(samples)'''
	stream = []
	integrator = 0

	for sample in raw:
		error = sample - integrator
		if error >= 0:
			stream.append(1)
			integrator = integrator + delta
		else:
			stream.append(0)
			integrator = integrator - delta
		
		# Clamp to signed 16 bit
		integrator = max(integrator, MIN)
		integrator = min(integrator, MAX)

	return stream


def decode_dm(stream, delta = MAX//21):
	"""
	Decodes a Delta Modulation BitStream in Signed 16 bit Audio data in a
	ByteArray.
	Keywords arguments:
		stream -- List with the BitStream
		delta -- Delta constant used in codification
	"""

	audio = []#array.array('h')
	integrator = 0

	for bit in stream:
		if bit:
			integrator = integrator + delta
		else:
			integrator = integrator - delta

		# Clamp to signed 16 bit
		integrator = max(integrator, MIN)
		integrator = min(integrator, MAX)
		audio.append(integrator)

	return audio#.tobytes()