import matplotlib.pyplot as plt
import numpy as np
from scipy import special as sp

# Q-function
def qfunc(x):
    return 0.5-0.5*sp.erf(x/np.sqrt(2))
    

# BER
def ber_calc(M, SNR):
    pspn = 10**(SNR/10)
    q_arg = np.sqrt((6*2*pspn)/((M**2)-1))
    ret = 2 * (1 - (1/M)) * qfunc(q_arg)
    return ret

ber_curve = []
snr_db = np.arange(-5, 15, 1)
for sn in snr_db:
    ber_curve.append(ber_calc(4, sn))

# plotting the points
plt.plot(snr_db, ber_curve)

# naming the x axis
plt.xlabel('SNR[db]')
# naming the y axis
plt.ylabel('BER')

# giving a title to my graph
plt.title('Bit Error Rate (BER)')

plt.grid()

# function to show the plot
plt.show()

    