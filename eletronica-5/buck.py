from printSI import printSI
from capacitor import getCap

from nucleosInd import dataNucleo,dataFio
from numpy import pi

trabalho = ''

#trabalho GB
I_o = 10 #A
V_o = 25 #V +/- 0,1 V
delta_Vc = 0.1 #V
V_in = 170 #Volts fixo.(sem faixa de valor máximo e mnimo)
Vce_sat = 1 #V
V_d = 0.5 #V


IAC_PICO = I_o	#A

#dados adotados pelo projetista
freq = 100e3 # kHz 
B = 0.3
Ku = 0.4
µ0 = 4*pi*1e-7
Tamp = 20
dT = 30	#°C


print('')
print('*************************')
titulo = '\nParâmetros do Projeto\n'
print(titulo)
trabalho += titulo
trabalho += printSI(freq,'freq','k','Hz',latexEQU = 'freq')
trabalho += printSI(I_o,'I_o','','A',latexEQU = 'I_o')
trabalho += printSI(V_o,'V_o','','V',latexEQU = 'V_o')
trabalho += printSI(delta_Vc,'delta_Vc','','V',latexEQU = 'delta_Vc')
trabalho += printSI(V_in,'V_in','','V',latexEQU = 'V_in')
print('')
print('*************************')
titulo = '\nInformações do Projeto\n'
print(titulo)
trabalho += titulo
trabalho += printSI(Vce_sat,'Vce_sat','','V',latexEQU = 'Vce_sat')
trabalho += printSI(V_d,'V_d','','V',latexEQU = 'V_d')

trabalho += printSI(IAC_PICO,'IAC_PICO','','A',latexEQU = 'IAC_PICO')
trabalho += printSI(dT,'dT','','°C',latexEQU = 'dT')
trabalho += printSI(B,'B','','T',latexEQU = 'B')
trabalho += printSI(Ku,'Ku','','',latexEQU = 'Ku')


print('*************************')
titulo = '\nCalculado\n'
print(titulo)
trabalho += titulo


Vin_Max = V_in*1.10
trabalho += printSI(Vin_Max,'Vin_Max','','V',latexEQU = 'Vin_Max = V_in*1.10')

Vin_Min = V_in*0.90
trabalho += printSI(Vin_Min,'Vin_Min','','V',latexEQU = 'Vin_Min = V_in*0.90')

Io_Min = I_o*0.1
trabalho += printSI(Io_Min,'Io_Min','','A',latexEQU = 'Io_Min = I_o*0.1')

d_Max =  (V_o + V_d) / ( Vin_Max - Vce_sat + V_d)
trabalho += printSI(d_Max,'d_Max','%','',latexEQU = 'd_Max =  (V_o + V_d) / ( Vin_Max - Vce_sat + V_d)')

d_Min = (V_o + V_d) / ( Vin_Max - Vce_sat + V_d)
trabalho += printSI(d_Min,'d_Min','%','',latexEQU = 'd_Min = (V_o + V_d) / ( Vin_Max - Vce_sat + V_d)')


L = (d_Min*(1-d_Min)*Vin_Max*(1/freq)) / (2*Io_Min)
trabalho += printSI(L,'L','µ','H',latexEQU = 'L = (d_Min*(1-d_Min)*Vin_Max*(1/freq)) / (2*Io_Min)')

C = 10* (d_Min*(1-d_Min)*Vin_Max) / (8*L*delta_Vc*freq**2)
trabalho += printSI(C,'C','µ','F',latexEQU = 'C = 10* (d_Min*(1-d_Min)*Vin_Max) / (8*L*delta_Vc*freq**2)')

titulo = '\nPassando para valores comerciais\n'
print(titulo)
trabalho += titulo

C = getCap(C)
trabalho += printSI(C,'C','µ','F',latexEQU = 'C_final')






def dimensiona(index, show = True):
	latex = ''
	# Carrega dasdos escolhidos
	if(show): print('Núcleo Escolhido:')
	if(show): print(list(dataNucleo)[index])
	AP,CEM,Le,Ae,As,Kj,X,Caminhos = list(dataNucleo.values())[index]
	latex += printSI(AP,'AP','cm^4','',output = show,latexEQU = 'AP')
	latex += printSI(CEM,'CEM','c','m',output = show,latexEQU = 'CEM')
	latex += printSI(Le,'Le','c','m',output = show,latexEQU = 'Le')
	latex += printSI(Ae,'Ae','cm²','',output = show,latexEQU = 'Ae')
	latex += printSI(As,'As','cm²','',output = show,latexEQU = 'As')
	latex += printSI(Kj,'Kj','','',output = show,latexEQU = 'Kj')
	latex += printSI(X,'X','','',output = show,latexEQU = 'X')

	# 2 - Calculo do Kj
	Kj = Kj*(dT**0.54)
	Z = 1/(1-X)

	# 3 - Calculo do Ap
	AP_calc = ((2*E*1e4)/(Ku*Kj*B))**Z/1e8
	latex += printSI(AP_calc,'AP_calc','cm^4','',output = show,latexEQU = 'AP_calc = ((2*E*1e4)/(Ku*Kj*B))**Z/1e8')

	# 4 - Calculo do AL
	Al =  (Ae**2)*(B**2)/2/E
	latex += printSI(Al,'Al','n','H/esp²',output = show,latexEQU = 'Al =  (Ae**2)*(B**2)/2/E')

	# 5 - Calculo do GAP
	µe = Al*Le/µ0/Ae
	latex += printSI(µe,'µe','','',output = show,latexEQU = 'µe = Al*Le/µ0/Ae')

	lg = Le/µe
	latex += printSI(lg,'lg','m','m',output = show,latexEQU = 'lg = Le/µe')

	gap =lg/Caminhos
	latex += printSI(gap,'gap','m','m',output = show,latexEQU = 'gap =lg/Caminhos')

	# 6 - Calculo do Nº de espiras
	N = round((L/Al)**(1/2),0)
	latex += printSI(N,'N','','',0,output = show,latexEQU = 'N = round((L/Al)**(1/2),0)')

	# 7 - Calculo do Fio
	J = Kj*(AP_calc*1e8)**(-X)
	latex += printSI(J,'J','','A/cm²',output = show,latexEQU = 'J = Kj*(AP_calc*1e8)**(-X)')

	Acu = I_o/J/1e4
	latex += printSI(Acu,'Acu','cm²','',output = show,latexEQU = 'Acu = I_o/J/1e4')

	γ = (4.35e3/freq)**(1/2)/1e3
	latex += printSI(γ,'γ','m','m',output = show,latexEQU = 'γ = (4.35e3/freq)**(1/2)/1e3')

	DiaUtil = γ*2
	latex += printSI(DiaUtil,'DiaUtil','m','m',output = show,latexEQU = 'DiaUtil = γ*2')

	size = len(list(dataFio))
	for x in range(size):
		D,A,D_Isol,A_Isol,Ω_by_m_20C,Ω_by_m_100C= list(dataFio.values())[size-x-1]
		
		if(DiaUtil<=D):
			if(show): print('Utilizar fio',list(dataFio)[size-x-1],'AWG')
			areaFio = A
			break

	latex += printSI(A,'A','cm²','',output = show,latexEQU = 'X')
	Elementos = int(Acu/A)
	latex += printSI(Elementos,'Elementos','','',0,output = show,latexEQU = 'Elementos = int(Acu/A)')


	#8 - Perdas
	deltaB = N*Al*IAC_PICO*2/Ae
	latex += printSI(deltaB,'deltaB','','T',output = show,latexEQU = 'deltaB = N*Al*IAC_PICO*2/Ae')

	Grafico = 50e-3#80e-3
	latex += printSI(Grafico,'Grafico','m','W/cm³',output = show,latexEQU = 'Grafico')

	PerdasNucleo = Grafico*Ae*Le*1e6
	latex += printSI(PerdasNucleo,'PerdasNucleo','','W',output = show,latexEQU = 'PerdasNucleo = Grafico*Ae*Le*1e6')

	ComprimentoFio = CEM * N
	latex += printSI(ComprimentoFio,'ComprimentoFio','c','m',output = show,latexEQU = 'ComprimentoFio = CEM * N')

	ResTotal = Ω_by_m_20C * ComprimentoFio / Elementos
	latex += printSI(ResTotal,'ResTotal','m','Ω',output = show,latexEQU = 'ResTotal = Ω_by_m_20C * ComprimentoFio / Elementos')

	PerdasCobre = ResTotal*(I_o**2 + (IAC_PICO**(1/2))/2)
	latex += printSI(PerdasCobre,'PerdasCobre','','W',output = show,latexEQU = 'PerdasCobre = ResTotal*(I_o**2 + (IAC_PICO**(1/2))/2)')

	PerdasTotais = PerdasNucleo + PerdasCobre
	latex += printSI(PerdasTotais,'PerdasTotais','','W',output = show,latexEQU = 'PerdasTotais = PerdasNucleo + PerdasCobre')
	
	deltaT = (1.821e5*PerdasTotais/((82+Tamp)*As*1e4))**0.818
	latex += printSI(deltaT,'deltaT','','°C',output = show,latexEQU = 'deltaT = (1.821e5*PerdasTotais/((82+Tamp)*As*1e4))**0.818')
	
	return PerdasTotais,deltaT,latex
	

# 1 - Calculo da Energia
E = (L*(I_o+IAC_PICO)**2)/2
trabalho += printSI(E,'E','m','J',latexEQU = 'E = (L*(I_o+IAC_PICO)**2)/2')

# - Busca por Núcleos Válidos
print('Opção\tNome\t\tApCalc\tAp\tPerdas\tdT')
print('\t\t\tcm^4\tcm^4\tW\t°C')
for x in range(len(list(dataNucleo))):
	AP,CEM,Le,Ae,As,Kj,X,Caminhos = list(dataNucleo.values())[x]
	Z = 1/(1-X)
	Kj = Kj*(dT**0.54)
	AP_calc =((2*E*1e4)/(Ku*Kj*B))**Z/1e8

	if(AP_calc<AP):
		PerdasTotais,deltaT,_ = dimensiona(x,False)
	
		print(x,'\t',end="")
		print(list(dataNucleo)[x],'\t',end="")
		print(round(AP_calc*1e8,3),'\t',end="")
		print(round(AP*1e8,3),'\t',end="")
		print(round(PerdasTotais,3),'\t',end="")
		print(int(deltaT),'\t',end="")
		
		print('')

# - Captura escolha do Usuário
x = -1
while x <= 0 or x > len(list(dataNucleo)) :
	print('Digite um número no catalogo de nucleos compatíveis')
	a = input('').split(" ")[0]
	x = int(a)
_ , _ , t = dimensiona(x)
trabalho += t

print(trabalho)
