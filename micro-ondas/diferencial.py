from printSI import printSI
from numpy import log10, sqrt, log,pi


trabalho = ''
_ = 0

trabalho += 'Constantes de projeto\n'
Vout_min = 0.3		#- Vout mínimo (V): 0.3, 
Vout_max = 1.5		#- Vout máximo(V): 1.5
C_L = 1.00E-11			#- Carga capacitiva (F): 1.00E-11
GBW = 10e6				#- GBW (MHz): 10
SlewRate = 10E6	#- slew rate: 10E6 V/m
G = 40				#- Ganho de tensão Av min: 40 dB
V_comum = 0.9		#- VDC (V): 0.9 -- modo comum de saída.
Vdd = 1.8			#- VDD (V): 1.8, VSS (V) = 0V
						#- Technologia CMOS_180

trabalho += printSI(Vout_min,'Vout_min','','V')
trabalho += printSI(C_L,'C_L','p','F')
trabalho += printSI(GBW,'GBW','M','Hz')
trabalho += printSI(G,'G','','dB')
trabalho += printSI(V_comum,'V_comum','','V')
trabalho += printSI(Vdd,'Vdd','','V')


trabalho += 'Para definir a corrente $I_{SS}$\n'


I_SS = SlewRate*C_L
trabalho += printSI(I_SS,'I_SS','µ','A',latexEQU = 'I_SS = SlewRate*C_L')


g_m = 2*pi*C_L*GBW
trabalho += printSI(g_m,'g_m','m','S',latexEQU = 'g_m = 2*pi*C_L*GBW')

trabalho += 'Para calcular as tensões de polariação usou-se $V_TH=0.5V$ baseado nos trabalhos anteriores'

trabalho += printSI(_,'_','','',latexEQU = 'Vout_min > V_GS1 - V_TH')

trabalho += printSI(_,'_','','V',latexEQU = 'V_GS1 = V_GS2  < 0.8')


trabalho += 'Para o espelho de corrente, utiliza-se o $Vout_max$'
trabalho += printSI(_,'_','','',latexEQU = '(Vout_max)>(V_GS3)-V_TH')
trabalho += printSI(_,'_','','',latexEQU = 'VDD - Vout_max >V_S2-V_G3-V_TH')
trabalho += printSI(_,'_','','V',latexEQU = 'V_G3>1')


trabalho += printSI(_,'_','','',latexEQU = 'V_DS5>V_GS5-V_TH')
trabalho += printSI(_,'_','','V',latexEQU = 'V_GS5<0.65')



print(trabalho)