from printSI import printSI
from numpy import log10, sqrt, log

trabalho = ''

# Constantes de projeto
k = 1.3806503e-23 #J/K
T = 300 #Kelvin

print('Receptor Bluetooth Low Energy')
Banda = 2.4E9 #Hz
bandwidth = 1E6 #Hz
P_sig = -103 #dBm (Sensibilidade)
SNRout = 8 #dB Pressuposto 
Z = 50 # ohm
swing = 0.5 #V
ΔPadj = -20#dB Potência Adjacente
P_Teste = -67 #dBm Não sei de onde o Sandro tirou esse valor



print('Calcular o Requisito de Ganho')
Vp = sqrt(10**(P_sig/10)*Z*2*1e-3)
trabalho += printSI(Vp,'Vp','µ','V',latexEQU = 'Vp = sqrt(10**(P_sig/10)*Z*2*1e-3)')

ganho = 20*log10(swing/Vp)
trabalho += printSI(ganho,'ganho','','dB',latexEQU = 'ganho = 20*log10(swing/Vp)')


print('Calcular a figura de Ruído')
NF = P_sig- 10*log10(k*T*bandwidth/1E-3)-SNRout
trabalho += printSI(NF,'NF','','dB',latexEQU = 'NF = P_sig- 10*log10(k*T*bandwidth/1E-3)-SNRout')


print('Calcular A Linearidade')



#print('Outra Opção de IIP3')
P_int = P_Teste - ΔPadj #dB  Adjacent (1 MHz) interference to in-band image frequency
trabalho += printSI(IM_3,'IM_3','','dB',latexEQU = 'IM_3 =  10*log10(k*T*bandwidth) + NF')

IM_3 =  10*log10(k*T*bandwidth/1E-3) + NF
trabalho += printSI(IM_3,'IM_3','','dB',latexEQU = 'IM_3 =  10*log10(k*T*bandwidth) + NF')

IIP_3 = (3*P_int - IM_3)/2
trabalho += printSI(IIP_3,'IIP_3','','dB',latexEQU = 'IIP_3 = (3*P_int - IM_3)/2')




print('Parameters Specifications')

'''
Design of a 2.4 GHz CMOS LNA for Bluetooth
Low Energy Application Using 45 nm Technology
Chin-To Hsiao
San Jose State University

Power Supply 1-3 V
Power Dissipation 4-20mW
Input and Output Impedance 50 Ω
Gain > 13.4 dB
NF < 3 dB
IIP3 > -10 dBm
'''

print(trabalho)
