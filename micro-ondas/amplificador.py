'''
como o boa prática, podemos adotar um L cinco vezes maior que o mínimo para a tecnologia'''
from printSI import printSI
from numpy import pi

trabalho = ''

g_m  = 0
C_L = 5e-12

GBW = 50e6
trabalho += printSI(GBW,'GBW','','',latexEQU = 'GBW = g_m*1.2/(2*pi*C_L)')


g_m = 2*C_L*pi*GBW
trabalho += printSI(g_m,'g_m','m','S',latexEQU = 'g_m = 2*C_L*pi*GBW')


print(trabalho)