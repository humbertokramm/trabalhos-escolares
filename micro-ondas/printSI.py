from pytexit import py2tex
import re

EngNotation ={
	'Y':	1e24,
	'Z':	1e21,
	'E':	1e18,
	'P':	1e15,
	'T':	1e12,
	'G':	1e9,
	'M':	1e6,
	'k':	1e3,
	'h':	1e2,
	'da':	1e1,
	'' :	1e0,
	'd':	1e-1,
	'c':	1e-2,
	'm':	1e-3,
	'u':	1e-6,
	'µ':	1e-6,
	'n':	1e-9,
	'p':	1e-12,
	'f':	1e-15,
	'a':	1e-18,
	'z':	1e-21,
	'y':	1e-24,
	#extras
	'cm^4':	1e-8,
	'cm^3':	1e-6,
	'cm^2':	1e-4,
	'cm³':	1e-6,
	'cm²':	1e-4,
	'mm^4':	1e-12,
	'mm^3':	1e-9,
	'mm^2':	1e-6,
	'mm³':	1e-9,
	'mm²':	1e-6,
	'%':	1e-2
}

GreekLetters =[
	['Ω',	'\Omega '],
	['Δ',	'\Delta '],
	#['λ',	'\lambda '], #isso não funciona, pois lambda é uma palavra reservada do python
	['π',	'\pi '],
	[' pi ',	'\pi '],
	['µ',	'\mu ']
	# to do
	# descreva outras letras aqui
]


def eng2float(s):
	notation = s[:-1] 
	matches = re.findall(r"[-+]?\d*\.\d+|\d+", s)
	if(len(matches)>0):
		value = float(matches[-1])
		for i in range (len(matches)):
			s = s.replace(str(matches[i]), '')
			s = s.replace('\t', ' ')
		s = s.split(' ')
		s = [st for st in s if st != '']
		if(len(s)>0):
			if s[-1] in EngNotation:
				value *= EngNotation[s[-1]]
			else: print(s[-1],' is not a valid notation')
	else: value = 'nan'
	return value

def GreekReplace(str):
	for i in range (len(GreekLetters)):
		str = str.replace(GreekLetters[i][0], GreekLetters[i][1])
	return str


def py2latex(string,var,res):
	res = GreekReplace(res)
	output = '\n\\begin{equation}'+'\n'
	output += py2tex(string,print_latex=False,print_formula=False)[2:-2]
	output += res+'\n'
	output += '\\label{equ: '
	output += var
	output += '}'+'\n'
	output += '\\end{equation}'+'\n'
	return output
	
	
def printSI(var,varname = 'var', expo = '',unit = '',rounded = 3,output = True,latexEQU = ''):
	if(rounded == 0):
		result = ' = ' + str(int(var/EngNotation[expo])) + ' ' + expo + unit
	else:
		result = ' = ' + str(round(var/EngNotation[expo],rounded)) + ' ' + expo + unit
	name = varname + result
	if(output):
		print(name)
		if(len(latexEQU)>1):
			name = py2latex(latexEQU,varname,result)
	else:
		name = '\n'
	return name
